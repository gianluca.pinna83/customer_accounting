<?php

namespace Tests\Domain\Entities; 

use Tests\TestCase;
use App\Domain\Entities\Accounting as AccountingEntity;

class AccountingTest extends TestCase
{
    
    public function testComputeBalanceOnDeposit() : void
    {
        $accounting = new AccountingEntity();
        
        $accounting->createDeposit(100);

        $this->assertEquals(100, $accounting->getBalance());
    }


    public function testComputeBalanceOnWithdrawal() : void
    {
        $accounting = new AccountingEntity();
        
        $this->setPrivateProperty($accounting, 'balance', 100);

        $accounting->createWithdrawal(30);

        $this->assertEquals(70, $accounting->getBalance());
    }


    public function testApplyBonusEveryThirdDeposit() : void
    {

        $accounting = new AccountingEntity();

        $this->setPrivateProperty($accounting,'bonus_percentage', 10);

        // deposit no 1
        $accounting->createDeposit(100);
        $this->assertEquals(0, $accounting->getBonusBalance());

        // deposit no 2
        $accounting->createDeposit(100);
        $this->assertEquals(0, $accounting->getBonusBalance());

        // deposit no 3
        $accounting->createDeposit(100);
        $this->assertEquals(10, $accounting->getBonusBalance());
        $this->assertEquals(310, $accounting->getBalance());



        // deposit no 4
        $accounting->createDeposit(100);
        $this->assertEquals(10, $accounting->getBonusBalance());

        // deposit no 5
        $accounting->createDeposit(100);
        $this->assertEquals(10, $accounting->getBonusBalance());

        // deposit no 6
        $accounting->createDeposit(77);
        $this->assertEquals(17.7, $accounting->getBonusBalance());
        $this->assertEquals(594.7, $accounting->getBalance());

    }



    public function testCannotWithdrawalMoreThanRealMoneyBalance()
    {
        $accounting = new AccountingEntity();

        $this->setPrivateProperty($accounting, 'balance', 100);
        $this->setPrivateProperty($accounting, 'bonus_balance', 10);

        $this->assertEquals(110, $accounting->getBalance());

        $this->expectException('Exception');
        $accounting->createWithdrawal(107);

    }
}
