<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) { return $router->app->version(); });

$router->post('/customer', ['as' => 'create-customer', 'uses' => 'CustomerController@create']);

$router->put('/customer/{customerId}', ['as' => 'update-customer', 'uses' => 'CustomerController@update']);

$router->post('/deposit', ['as' => 'create-deposit', 'uses' => 'AccountingController@createDeposit']);

$router->post('/withdrawal', ['as' => 'create-withdrawal', 'uses' => 'AccountingController@createWithdrawal']);

$router->get('/accounting-report', ['as' => 'read-accounting-report', 'uses' => 'AccountingReportController@read']);