<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\Accounting;

class CustomerCreatedListener
{
    
    /**
     * @var Accounting
     */
    private $service;


    public function __construct(Accounting $service)
    {
        $this->service = $service;
    }

    
    public function handle(CustomerCreated $event) : void
    {
       $customer = $event->getCustomer();

       $this->service->create( $customer );
    }
}
