<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Exception;
use Doctrine\DBAL\Connection;
use App\Services\Customer as CustomerService;
use Validator;

class CustomerController extends Controller
{

    /**
     * @var CustomerService
     */
    protected $service;


    
    public function __construct(CustomerService $service)
    {
        $this->service = $service;
    }


    
    public function create(Request $request) : \Illuminate\Http\JsonResponse
    {

        /** @var Connection $connection */
        $connection = app('em')->getConnection();

        try {

            $connection->beginTransaction();

            $this->validateRequest($request);
            
            $result = $this->service->create( $request->json()->all() );

            $connection->commit();

            return response()->json(
                array_merge(['success' => true, 'data' => $result])
            );

        } catch(Exception $e) {

            $connection->rollBack();

            return response()->json(
                [ 'success' => false, 'message' => $e->getMessage()]
            );
        }
    }



    public function update(Request $request, $customerId) : \Illuminate\Http\JsonResponse
    {

        /** @var Connection $connection */
        $connection = app('em')->getConnection();

        try {

            $connection->beginTransaction();

            $this->validateRequest($request);
            
            $result = $this->service->update($customerId, $request->json()->all() );

            $connection->commit();

            return response()->json(
                array_merge(['success' => true, 'data' => $result])
            );

        } catch(Exception $e) {

            $connection->rollBack();

            return response()->json(
                [ 'success' => false, 'message' => $e->getMessage()]
            );
        }
    }



    public function validateRequest(Request $request) : bool
    {
        $params = $request->json()->all();
        $rules = [
            'email' => 'email'
        ];

        $validator = Validator::make($params, $rules);
        if ($validator->passes()) return true;
        
        throw new Exception( json_encode($validator->errors()->all()) );
        
    }

}