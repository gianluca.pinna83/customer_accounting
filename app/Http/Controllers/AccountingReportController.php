<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Accounting as AccountingService;
use Validator;
use \Exception;


class AccountingReportController extends Controller
{

    /**
     * @var AccountingService
     */
    protected $service;


    
    public function __construct(AccountingService $service)
    {
        $this->service = $service;
    }



    public function read(Request $request) : \Illuminate\Http\JsonResponse
    {
        try {

            $this->validateRequest($request);
            
            $result = $this->service->getTotals( $request->all() );

            return response()->json(
                array_merge(['success' => true, 'data' => $result])
            );

        } catch(Exception $e) {

            return response()->json(
                [ 'success' => false, 'message' => $e->getMessage()]
            );
        }
    }





    public function validateRequest(Request $request) : bool
    {
        $params = $request->all();
        $rules = [
            'from_date' => 'date_format:Y-m-d|required_with:to_date',
            'to_date' => 'date_format:Y-m-d|required_with:from_date|after_or_equal:from_date'
        ];

        $validator = Validator::make($params, $rules);
        if ($validator->passes()) return true;
        
        throw new Exception( json_encode($validator->errors()->all()) );
        
    }
}