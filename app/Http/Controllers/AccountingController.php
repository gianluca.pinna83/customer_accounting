<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \Exception;
use Doctrine\DBAL\Connection;
use App\Services\Accounting as AccountingService;
use Validator;


class AccountingController extends Controller
{

    /**
     * @var AccountingService
     */
    protected $service;


    
    public function __construct(AccountingService $service)
    {
        $this->service = $service;
    }



    public function createDeposit(Request $request) : \Illuminate\Http\JsonResponse
    {
        /** @var Connection $connection */
        $connection = app('em')->getConnection();

        try {

            $connection->beginTransaction();

            $this->validateRequest($request);
            
            $result = $this->service->createDeposit( $request->json()->all() );

            $connection->commit();

            return response()->json(
                array_merge(['success' => true, 'data' => $result])
            );

        } catch(Exception $e) {

            $connection->rollBack();

            return response()->json(
                [ 'success' => false, 'message' => $e->getMessage()]
            );
        }
    }



    public function createWithdrawal(Request $request) : \Illuminate\Http\JsonResponse
    {
        /** @var Connection $connection */
        $connection = app('em')->getConnection();

        try {

            $connection->beginTransaction();

            $this->validateRequest($request);
            
            $result = $this->service->createWithdrawal( $request->json()->all() );

            $connection->commit();

            return response()->json(
                array_merge(['success' => true, 'data' => $result])
            );

        } catch(Exception $e) {

            $connection->rollBack();

            return response()->json(
                [ 'success' => false, 'message' => $e->getMessage()]
            );
        }
    }



    public function validateRequest(Request $request) : bool
    {
        $params = $request->json()->all();
        $rules = [
            'customer_id' => 'required',
            'amount' => 'required|numeric'
        ];

        $validator = Validator::make($params, $rules);
        if ($validator->passes()) return true;
        
        throw new Exception( json_encode($validator->errors()->all()) );
        
    }
}