<?php 

namespace App\Domain\Entities;

use Doctrine\ORM\Mapping AS ORM;
use ReflectionClass;
use ReflectionMethod;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer",uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 */
class Customer
{
	 /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
	

    /**
     * @ORM\Column(type="string")
     */
    private $gender;
	
	
	/**
     * @ORM\Column(type="string")
     */
	private $first_name;
	
	
	/**
     * @ORM\Column(type="string")
     */
	private $last_name;
	
	
	/**
     * @ORM\Column(type="string")
     */
	private $country;
	
	
	/**
     * @ORM\Column(type="string")
     */
	private $email;
	
	
	
	public function getId() : int
	{
		return $this->id;
	}
		
	
	public function getGender() : string
	{
		return $this->gender;
	}
	
	
	public function getFirstName() : string
	{
		return $this->first_name;
	}
	
	
	public function getLastName() : string
	{
		return $this->last_name;
	}
	
	
	public function getCountry() : string
	{
		return $this->country;
	}
	
	
	public function getEmail() : string
	{
		return $this->email;
	}
	
	
	
	public function setGender(string $gender) : Customer
	{
		$this->gender = $gender;
		return $this;
	}
	
	
	
	public function setFirstName(string $firstName) : Customer
	{
		$this->first_name = $firstName;
		return $this;
	}
	
	

	public function setLastName(string $lastName) : Customer
	{
		$this->last_name = $lastName;
		return $this;
	}
	
	

	public function setCountry(string $country) : Customer
	{
		$this->country = $country;
		return $this;
	}
	
	
	
	public function setEmail(string $email) : Customer
	{
		$this->email = $email;
		return $this;
	}	


	
	public function apply(array $data) : Customer
	{
		$class = new ReflectionClass($this);
        $methods = array_map(function($m){ return $m->name;}, $class->getMethods(ReflectionMethod::IS_PUBLIC));

		foreach( $data as $fieldName=>$value) {

			$setMethod = 'set' . str_replace('_', '', ucwords($fieldName, '_'));

			if (in_array($setMethod, $methods) and $value) {

				$this->$setMethod($value);
			}
		}

		return $this;
	}
}