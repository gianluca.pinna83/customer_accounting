<?php 

namespace App\Domain\Entities;

use Doctrine\ORM\Mapping AS ORM;
use \DateTime;
use \DateTimezone;

/**
 * @ORM\Entity
 * @ORM\Table(name="deposit")
 */
class Deposit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;


    /**
     * @ORM\Column(type="float")
     */
    private $amount;


    /**
     * @ORM\ManyToOne(targetEntity="Accounting", inversedBy="deposits")
     */
    private $accounting;


    public function __construct(Accounting $accounting)
    {
        $this->accounting = $accounting;

        $this->date = new DateTime('now', new DateTimezone('UTC'));
    }


    public function setAmount(float $amount) : Deposit
    {
        $this->amount = $amount;
        return $this;
    }


    public function getAmount() : float
    {
        return $this->amount;
    }


    public function getDate() : Datetime
    {
        return $this->date;
    }


}