<?php 

namespace App\Domain\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \Exception;

/**
 * @ORM\Entity
 * @ORM\Table(name="accounting",uniqueConstraints={@ORM\UniqueConstraint(name="customer", columns={"customer_id"})})
 */
class Accounting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $customer_id;


    /**
     * @ORM\Column(type="decimal")
     */
    private $balance;

    /**
     * @ORM\Column(type="decimal")
     */
    private $bonus_balance;


    /**
     * @ORM\Column(type="decimal")
     */
    private $bonus_percentage;


    /**
     * @ORM\OneToMany(targetEntity="Deposit", mappedBy="accounting", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    public $deposits;

    /**
     * @ORM\OneToMany(targetEntity="Withdrawal", mappedBy="accounting", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    public $withdrawals;


	public function __construct()
    {
        $this->deposits = new ArrayCollection(); 
        $this->withdrawals = new ArrayCollection();
    }


    public function init(int $customerId) : Accounting
    {
        $this->customer_id = $customerId;

        $this->balance = 0.0;
        $this->bonus_balance = 0.0;

        $this->bonus_percentage = rand(5, 20);

        return $this;
    }

    
    public function createDeposit($amount) : Accounting
    {

        $deposit = new Deposit($this);

        $deposit->setAmount($amount);

        $this->deposits->add($deposit);

        $this->balance += $deposit->getAmount();

        $bonusBalance = $this->computeBonusBalance($deposit);

        $this->bonus_balance += $bonusBalance;
        
        return $this;
    }

    

    public function createWithdrawal($amount) : Accounting
    {

        $withdrawal = new Withdrawal($this);

        $withdrawal->setAmount($amount);

        $this->withdrawals->add($withdrawal);

        $this->balance -= $withdrawal->getAmount();

        
        //check balance 
        if ($this->balance < 0) {
            throw new Exception('No available balance');
        }


        return $this;
    }


    public function getCustomerId()
    {
        return $this->customer_id;
    }

    public function getBalance()
    {
        return $this->balance + $this->bonus_balance;
    }


    public function getBonusBalance()
    {
        return $this->bonus_balance;
    }


    private function computeBonusBalance(Deposit $deposit) : int|float
    {
        if ($this->deposits->count() % 3 == 0) {
            return ($deposit->getAmount() * $this->bonus_percentage) / 100;
        }
        return 0;
    }

}