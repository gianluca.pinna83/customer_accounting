<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\Customer as CustomerEntity;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use \Exception;
use App\Events\CustomerCreated;

class Customer
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;


    /**
     * @var string
     */
    private $entityName = 'App\Domain\Entities\Customer';



    public function __construct()
    {
        $this->em = app('em');
        $this->repository = $this->em->getRepository($this->entityName);
    }


    
    public function create(CustomerEntity $customer) : void
    {
        try {

            $this->em->persist($customer);
            $this->em->flush();

        } 
        catch (UniqueConstraintViolationException $e)  {
            throw new Exception('Unique constaint violation on email: ' . $customer->getEmail());
        }
        catch (Exception $e) {
            throw $e;
        }


        event(new CustomerCreated($customer));
    }



    public function read($customerId) : CustomerEntity
    {
        $customer = $this->repository->find($customerId);
        if (null === $customer) throw new Exception('Not found');

        return $customer;
    }


    
    public function update(CustomerEntity $customer) : void
    {
        try {
            $this->em->flush();
        } 
        catch (UniqueConstraintViolationException $e)  {
            throw new Exception('Unique constaint violation on email: ' . $customer->getEmail());
        }
        catch (Exception $e) {
            throw $e;
        }
    }

}