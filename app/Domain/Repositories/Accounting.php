<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\Accounting as AccountingEntity;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use \Exception;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\NativeQuery;
use \DateTime;
use \DateTimezone;

class Accounting
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;


    /**
     * @var string
     */
    private $entityName = 'App\Domain\Entities\Accounting';



    public function __construct()
    {
        $this->em = app('em');
        $this->repository = $this->em->getRepository($this->entityName);
    }


    
    public function create(AccountingEntity $accounting) : void
    {
        try {

            $this->em->persist($accounting);
            $this->em->flush();

        } 
        catch (UniqueConstraintViolationException $e)  {
            throw new Exception('Unique constaint violation on customer_id');
        }
        catch (Exception $e) {
            throw $e;
        }
    }



    public function getAccountingByCustomer(int $customerId) : AccountingEntity
    {
        $accounting = $this->repository->findOneBy(['customer_id' => $customerId]);
        if (null === $accounting) throw new Exception('Not found');

        return $accounting;
    }


    public function update(AccountingEntity $accounting) : void
    {
        $this->em->flush();
    }


    public function getTotals(array $params) : array
    {

        $sql = "
            select 
                DATE(_accounting_operations.date) as date,
                _accounting_operations.country, 
                COUNT(distinct _accounting_operations.customer_id) as unique_customers,
                _deposits_totals.no_of_deposits,
                _deposits_totals.total_deposit_amount,
                _withdrawals_totals.no_of_withdrawals,
                _withdrawals_totals.total_withdrawal_amount

            from (
                select 
                deposit.date,
                customer.country,
                customer.id as customer_id
                from deposit
                inner join accounting on deposit.accounting_id = accounting.id
                inner join customer on accounting.customer_id = customer.id
                
                union all
                
                select 
                withdrawal.date,
                customer.country,
                customer.id as customer_id
                from withdrawal
                inner join accounting on withdrawal.accounting_id = accounting.id
                inner join customer on accounting.customer_id = customer.id
            ) as _accounting_operations

            left join (
                select COUNT(deposit.id) as no_of_deposits, SUM(deposit.amount) as total_deposit_amount, country, DATE(date) as date
                from deposit 
                inner join accounting on deposit.accounting_id = accounting.id
                inner join customer on accounting.customer_id = customer.id
                group by country, DATE(date)
            ) as _deposits_totals on _accounting_operations.country = _deposits_totals.country and DATE(_accounting_operations.date) = DATE(_deposits_totals.date)

            left join (
                select COUNT(withdrawal.id) as no_of_withdrawals, -SUM(withdrawal.amount) as total_withdrawal_amount, country, DATE(date) as date
                from withdrawal 
                inner join accounting on withdrawal.accounting_id = accounting.id
                inner join customer on accounting.customer_id = customer.id
                group by country, DATE(date)
            ) as _withdrawals_totals on _accounting_operations.country = _withdrawals_totals.country and DATE(_accounting_operations.date) = DATE(_withdrawals_totals.date)
            
            where DATE(_accounting_operations.date) between :fromDate and :toDate

            group by _accounting_operations.country, DATE(_accounting_operations.date)

        ";

        $rsm = new ResultSetMapping;
        $fields = ['date', 'country', 'unique_customers', 'no_of_deposits', 'total_deposit_amount', 'no_of_withdrawals', 'total_withdrawal_amount'];

        foreach($fields as $field) {
            $rsm->addScalarResult($field, $field);
        }

        $query = $this->em->createNativeQuery($sql, $rsm);


        $this->applyDateFilter($params, $query);

        return $query->execute();

    }


    private function applyDateFilter(array $params, NativeQuery $query) : void
    {
        
        $now = new DateTime('now', new DateTimezone('UTC'));
        $sevenDaysAgo = new DateTime('7 days ago', new DateTimezone('UTC'));

        $fromDate = $params['from_date'] ?? $sevenDaysAgo->format('Y-m-d');
        $toDate = $params['to_date'] ?? $now->format('Y-m-d');

        
        $query->setParameters([
            'fromDate' => $fromDate,
            'toDate' => $toDate
        ]);
    }

}