<?php

namespace App\Services;

use App\Domain\Repositories\Accounting as AccountingRepository;
use App\Domain\Entities\Accounting as AccountingEntity;
use App\Domain\Entities\Customer as CustomerEntity;


class Accounting
{
 
    /**
     * @var AccountingRepository
     */
    private $repository;


    public function __construct(AccountingRepository $repository)
    {
        $this->repository = $repository;
    }


    
    public function create(CustomerEntity $customer) : void
    {
        $accounting = new AccountingEntity();

        $accounting->init( $customer->getId());

        $this->repository->create($accounting);
    }



    public function createDeposit(array $params) : array
    {
        $accounting = $this->repository->getAccountingByCustomer($params['customer_id']);

        $accounting->createDeposit($params['amount']);

        $this->repository->update($accounting);

        return [
            'customer_id' => $accounting->getCustomerId(),
            'balance' => $accounting->getBalance(),
            'bonus_balance' => $accounting->getBonusBalance()
        ];
    }


    public function createWithdrawal(array $params) : array
    {
        $accounting = $this->repository->getAccountingByCustomer($params['customer_id']);

        $accounting->createWithdrawal($params['amount']);

        $this->repository->update($accounting);

        return [
            'customer_id' => $accounting->getCustomerId(),
            'balance' => $accounting->getBalance(),
            'bonus_balance' => $accounting->getBonusBalance()
        ];
    }


    public function getTotals(array $params) : array
    {
        return $this->repository->getTotals($params);
    }

}