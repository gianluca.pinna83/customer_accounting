<?php

namespace App\Services;

use App\Domain\Repositories\Customer as CustomerRepository;
use App\Domain\Entities\Customer as CustomerEntity;


class Customer
{

    /**
     * @var CustomerRepository
     */
    private $repository;


    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }


    
    public function create(array $params) : array
    {
        $customer = new CustomerEntity;
        
        $customer->apply($params);

        $this->repository->create($customer);

        return ['customer_id' => $customer->getId()];
    }



    public function update(int $customerId, array $params) : array
    {
        $customer = $this->repository->read($customerId);
        
        $customer->apply($params);

        $this->repository->update($customer);

        return ['customer_id' => $customer->getId()];
    }
}
