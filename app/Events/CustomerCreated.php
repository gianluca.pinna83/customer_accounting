<?php

namespace App\Events;

use App\Domain\Entities\Customer;

class CustomerCreated extends Event
{

    /**
     * @var Customer
     */
    private $customer;


    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }


    public function getCustomer() : Customer
    {
        return $this->customer;
    }
}
