<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20210919141848 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE deposit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, accounting_id INTEGER DEFAULT NULL, date DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL);
        ");
        
        $this->addSql("
            CREATE TABLE withdrawal (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, accounting_id INTEGER DEFAULT NULL, date DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL);
        ");

        $this->addSql("
            CREATE TABLE accounting (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, customer_id INTEGER NOT NULL UNIQUE, balance DOUBLE PRECISION NOT NULL, bonus_balance DOUBLE PRECISION NOT NULL, bonus_percentage DOUBLE PRECISION NOT NULL);
        ");
    
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {

    }
}
