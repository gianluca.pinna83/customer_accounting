<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20210913090345 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {

        $this->addSql("
            CREATE TABLE accounting (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, customer_id INTEGER NOT NULL UNIQUE, balance NUMERIC(10, 0) NOT NULL, bonus_balance NUMERIC(10, 0) NOT NULL, bonus_percentage NUMERIC(10, 0) NOT NULL);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {

    }
}
