<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20210910125521 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE customer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, gender VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL UNIQUE);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {

    }
}
