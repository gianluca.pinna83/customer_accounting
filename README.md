# Customer Accounting

Author: Gian Luca Pinna

PHP Framework: Laravel Lumen 8

PHP Version: 8

Database: Sqlite

Installation:
 - clone the repository
 - run > composer update
 - run > php artisan doctrine:generate-proxies 
 - run > php -S localhost:8000 -t public


API
 - POST http://localhost:8000/customer (create a customer, example params: {"first_name":"Frank","last_name":"Pinna","gender":"Female","email":"frank.pinna@gmail.com","country":"France"} )

 - PUT http://localhost:8000/customer/{customer_id} ( edit customer)

 - POST http://localhost:8000/deposit ( example params: {"customer_id":"8","amount":"1000"})
 - POST http://localhost:8000/withdrawal ( example params: {"customer_id":"8","amount":"700"})

 - GET http://localhost:8000/accounting-report (optional params from_date, to_date. Both or none should be present with format Y-m-d)


 TESTS
- run: > vendor\bin\phpunit tests
